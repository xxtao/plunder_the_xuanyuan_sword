// Game.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Game.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGameApp

BEGIN_MESSAGE_MAP(CGameApp, CWinApp)
	//{{AFX_MSG_MAP(CGameApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGameApp construction

CGameApp::CGameApp()
{
	m_lLastRenewScreenTime = 0;
	m_lInactiveTime  = 0;
	m_lInactiveCount = 0;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGameApp object

CGameApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CGameApp initialization

BOOL CGameApp::InitInstance()
{
	// 防止同时运行多个程序
	HANDLE hMutex = CreateMutex( NULL, TRUE, m_pszAppName );
	if( GetLastError() == ERROR_ALREADY_EXISTS )
		return FALSE;

	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;

	pFrame->LoadFrame( IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
		NULL);

	// The one and only window has been initialized, so show and update it.
	pFrame->ShowWindow(SW_SHOW);

	pFrame->UpdateWindow();
    pFrame->SetForegroundWindow();

	return TRUE;
}

int CGameApp::Run() 
{
    MSG msg;
    ZeroMemory( &msg, sizeof(msg) );
    while( msg.message!=WM_QUIT )
    {
        if( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
        {
            TranslateMessage( &msg );
            DispatchMessage( &msg );
        }
        else
		{
			GameProc();
        }
	}

	return 0;
}

void CGameApp::GameProc()
{
	// 如果程序窗口不是活动窗口，则不处理
	if( m_pMainWnd->GetActiveWindow() == NULL )
    {
		// 记下不活动期间的起始时间
		if( m_lInactiveTime == 0 )
			m_lInactiveTime = clock();

        return;
    }

	// 计算不活动期间的时间跨度
	if( m_lInactiveTime > 0 )
	{
		m_lInactiveCount += (clock() - m_lInactiveTime);
		m_lInactiveTime = 0;
	}

	// 取得当前时间
	long lNow = clock();

	// 减去窗口处于不活动状态的时间，得到系统已经运行的时间
	lNow -= m_lInactiveCount;

	// 看是否需要重绘画面
	if( (lNow - m_lLastRenewScreenTime) > RENEW_SCREEN_GAP )
	{
		((CMainFrame*)m_pMainWnd)->ActProc( lNow );
		((CMainFrame*)m_pMainWnd)->Paint();
		m_lLastRenewScreenTime = lNow;
	}
}

/////////////////////////////////////////////////////////////////////////////
// 支持透明色的绘制函数
void TransBlt( HDC hdc,int ox,int oy,int cx,int cy,HDC hMemDC,int sx,int sy,COLORREF colorkey )
{
//	COLORREF crOldBack = ::SetBkColor( hdc, RGB(255,255,255) );
//	COLORREF crOldText = ::SetTextColor( hdc, RGB(0,0,0) );
//	COLORREF crOldBackImg = ::SetBkColor( hMemDC, colorkey );
//	COLORREF crOldTextImg = ::SetTextColor( hMemDC, RGB(0,0,0) );

    ::SetBkColor( hMemDC, colorkey );
    ::SetTextColor( hMemDC, RGB(0,0,0) );

	HDC hMaskDC  = ::CreateCompatibleDC( hdc );

	HBITMAP hBmpMask = ::CreateBitmap( cx, cy, 1, 1, NULL );
	HBITMAP hOldBmp  = (HBITMAP)::SelectObject( hMaskDC, hBmpMask );

	::SetBkColor( hMaskDC, RGB(255,255,255) );
	::SetTextColor( hMaskDC, RGB(0,0,0) );

	BitBlt( hMaskDC, 0, 0, cx, cy, hMemDC, sx, sy, SRCCOPY );
	BitBlt( hdc, ox, oy, cx, cy, hMemDC, sx, sy, SRCINVERT );
	BitBlt( hdc, ox, oy, cx, cy, hMaskDC, 0, 0, SRCAND );
	BitBlt( hdc, ox, oy, cx, cy, hMemDC, sx, sy, SRCINVERT );

	hBmpMask = (HBITMAP)::SelectObject( hMaskDC, hOldBmp );
	::DeleteObject( hBmpMask );
	::DeleteDC( hMaskDC );

//	::SetBkColor( hdc, crOldBack );
//	::SetTextColor( hdc, crOldText );
//	::SetBkColor( hMemDC, crOldBackImg );
//	::SetTextColor( hMemDC, crOldTextImg );
}

// 检验一个点是否落在一个矩形区域内
BOOL PtInArea( int ptx, int pty, int x, int y, int w, int h )
{
	if( (ptx > x) && (ptx < x+w) &&(pty > y) && (pty < y+h) )
		return TRUE;
	return FALSE;
}

// 检验两个矩形区域是否相交
BOOL IsAreaCut( int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2 )
{
	if( ( abs( (x1+w1/2) - (x2+w2/2) ) < ( abs(w1+w2) / 2 ) ) &&
		( abs( (y1+h1/2) - (y2+h2/2) ) < ( abs(h1+h2) / 2 ) ) )
		return TRUE;
	return FALSE;
}

// 从字符串中取得一行
BOOL GetLine( CString& strReturn, CString& strSource, int k )
{
	int nStartPos = 0;
	int nEndPos = -1;

	for( int i=0; i<=k; i++ )
	{
		// 把上次查找的位置给开始位置
		nStartPos = nEndPos+1;

		// 查找分隔符'\n'
		nEndPos = strSource.Find( '\n', nStartPos );

		if( nEndPos == -1 )
		{
			if( strSource.GetLength() <= nStartPos )
				return FALSE;

			nEndPos = strSource.GetLength();
		}
	}

	// 将nStartPos和nEndPos之间的内容拷贝给temp
	CString strTemp = strSource.Mid( nStartPos, nEndPos-nStartPos );

	// 如果开始字符是“\n”，则去掉
	if (strcmp (strTemp.Left(1), "\n") == 0)
	{
		nStartPos ++;
		strTemp = strSource.Mid( nStartPos, nEndPos-nStartPos );
	}

	// 如果结束字符是\r也去掉
	if (strcmp (strTemp.Right(1), "\r") == 0)
	{
		strTemp = strSource.Mid( nStartPos, nEndPos-nStartPos-1 );
	}

	strReturn = strTemp;
	return TRUE;
}

// 从字符串中取得两个“;”之间的部分
BOOL GetSlice( CString& strReturn, CString& strSource, int k )
{
	int nStartPos = 0;
	int nEndPos = -1;

	for( int i=0; i<=k; i++ )
	{
		// 把上次查找的位置给开始位置
		nStartPos = nEndPos+1;

		// 查找分隔符';'
		nEndPos = strSource.Find( ';', nStartPos );

		if( nEndPos == -1 )
		{
			return FALSE;
		}
	}
 
	// 将nStartPos和nEndPos之间的内容拷贝给temp
	strReturn = strSource.Mid( nStartPos, nEndPos-nStartPos );
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// END
