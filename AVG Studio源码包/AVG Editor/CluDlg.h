#if !defined(AFX_CLUDLG_H__F3F755CF_7977_4B5C_A78B_027777ACF9F8__INCLUDED_)
#define AFX_CLUDLG_H__F3F755CF_7977_4B5C_A78B_027777ACF9F8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CluDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCluDlg dialog
#include "scene.h"

class CCluDlg : public CDialog
{
// Construction
public:
	CLU* m_pClu;
	CCluDlg(CWnd* pParent = NULL);   // standard constructor
private:
	void UpdatePrompt();
	BOOL CheckValue();
// Dialog Data
	//{{AFX_DATA(CCluDlg)
	enum { IDD = IDD_CLUDLG };
	CEdit	m_editPara4;
	CEdit	m_editPara3;
	CEdit	m_editPara2;
	CEdit	m_editPara1;
	CComboBox	m_comboCommand;
	int		m_nIndex;
	int		m_nPara1;
	int		m_nPara2;
	int		m_nPara3;
	int		m_nPara4;
	CString	m_strPara1;
	CString	m_strPara2;
	CString	m_strPara3;
	CString	m_strPara4;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCluDlg)
	public:
	virtual int DoModal(CLU* pClu);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCluDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCombo();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLUDLG_H__F3F755CF_7977_4B5C_A78B_027777ACF9F8__INCLUDED_)
